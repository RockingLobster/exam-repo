#include "Card.h"



Card::Card(const int & number, const Symbol & symbol , const Shading & shading , const Colour & colour)
	: m_number(number),m_symbol(symbol),m_shading(shading),m_colour(colour)
{
}

Card::Card(const Card & other)
{
	*this = other;
}

Card & Card::operator=(const Card & other)
{
	m_number = other.m_number;
	m_symbol = other.m_symbol;
	m_shading = other.m_shading;
	m_colour = other.m_colour;
	return *this;
}

std::string Card::getShading() const
{
	switch (m_shading)
	{
	case Shading::SOLID:
		return "Solid";
	case Shading::STRIPED:
		return "Striped";
	case Shading::OPEN:
		return "Open";
	}
}

std::string Card::getSymbol() const
{
	switch (m_symbol)
	{
	case Symbol::DIAMOND:
		return "Diamond";
	case Symbol::SQUIGGLE:
		return "Squiggle";
	case Symbol::OVAL:
		return "Oval";
	}
}

std::string Card::getColour() const
{
	switch (m_colour)
	{
	case Colour::RED:
		return "RED";
	case Colour::GREEN:
		return "GREEN";
	case Colour::BLUE:
		return "BLUE";
	}
}

int Card::getNumber() const
{
	return m_number;
}


Card::~Card()
{
}

std::ostream & operator<<(std::ostream & o, const Card & target)
{
	o << target.m_number << " " << target.getSymbol() << " " << target.getShading() << " " << target.getColour()<<std::endl;
	return o;
}
