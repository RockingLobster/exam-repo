#pragma once
#include "Deck.h"
#include "TheDealerTM.h"
#include <vector>
class GameBoard 
{
public:
	GameBoard(const int & numberOfPlayers=1);
	~GameBoard();
	void playRound();
	void printCurrentRound() const;
	bool validateSets(const int & index1, const int & index2, const int & index3) const;
private:
	std::vector<Card> cardsInGame;
	TheDealerTM Johnny;
    int numberOfPlayers;
	std::vector<int> currentPlayers;
};

