#include "GameBoard.h"



GameBoard::GameBoard(const int & numberOfPlayers) : numberOfPlayers(numberOfPlayers)
{
	currentPlayers.resize(numberOfPlayers);
	Johnny.shuffleDeck();
	for (size_t index = 0; index < 13; ++index)
	{
		cardsInGame.push_back(Johnny.dealCard());
	}
}



GameBoard::~GameBoard()
{
}

void GameBoard::playRound()
{
	int firstIndex;
	int secondIndex;
	int thirdIndex;
	std::cout << "current Board:" << std::endl;
	printCurrentRound();
		std::cout << "s if you found a set or p to pass"<<std::endl;
		char choice;
	
	while (cardsInGame.size() < 13)
	{
		cardsInGame.push_back(Johnny.dealCard());
	}
	for (int index = 0; index < numberOfPlayers; ++index)
	{
		std::cin >> choice;
		while (choice != 'p')
		{
			std::cout << "enter the indexes of the 3 cards you think are a set" << std::endl;
			std::cin >> firstIndex >> secondIndex >> thirdIndex;
			if (validateSets(firstIndex, secondIndex, thirdIndex) == true)
			{
				std::cout << "set validated"<<std::endl;

			}
			else
			{
				std::cout << "not a set >:["<<std::endl;
			}
			std::cout << "S to continue or p to pass"<<std::endl;
			std::cin >> choice;
		}
		std::cout << "next player" << std::endl;
		
	}
}

void GameBoard::printCurrentRound() const
{
	for (int index = 0; index < cardsInGame.size(); ++index)
	{
		std::cout << "[" << index << "]" << cardsInGame[index];
	}
}

bool GameBoard::validateSets(const int & index1, const int & index2, const int & index3) const
{
	if (cardsInGame[index1].getSymbol() == cardsInGame[index2].getSymbol() && cardsInGame[index1].getSymbol() == cardsInGame[index3].getSymbol())
	{
		return true;
	}
	if (cardsInGame[index1].getColour() == cardsInGame[index2].getColour() && cardsInGame[index1].getColour() == cardsInGame[index3].getColour())
	{
		return true;
	}
	if (cardsInGame[index1].getShading() == cardsInGame[index2].getShading() && cardsInGame[index1].getShading() == cardsInGame[index3].getShading())
	{
		return true;
	}
	if (cardsInGame[index1].getNumber() == cardsInGame[index2].getNumber() && cardsInGame[index1].getNumber() == cardsInGame[index3].getNumber())
	{
			return true;
	}
		
	if (cardsInGame[index1].getSymbol() != cardsInGame[index2].getSymbol() && cardsInGame[index1].getSymbol() != cardsInGame[index3].getSymbol() && cardsInGame[index3].getSymbol() != cardsInGame[index2].getSymbol())
	{
		return true;
	}
	if (cardsInGame[index1].getColour() != cardsInGame[index2].getColour() && cardsInGame[index1].getColour() != cardsInGame[index3].getColour() && cardsInGame[index3].getColour() != cardsInGame[index2].getColour())
	{
		return true;
	}
	if (cardsInGame[index1].getShading() != cardsInGame[index2].getShading() && cardsInGame[index1].getShading() != cardsInGame[index3].getShading() && cardsInGame[index3].getShading() != cardsInGame[index2].getShading())
	{
		return true;
	}
	if (cardsInGame[index1].getNumber() != cardsInGame[index2].getNumber() && cardsInGame[index1].getNumber() != cardsInGame[index3].getNumber() && cardsInGame[index3].getNumber() != cardsInGame[index2].getNumber())
	{
		return true;
	}
		
	
	return false;
}
