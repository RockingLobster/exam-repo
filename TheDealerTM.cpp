#include "TheDealerTM.h"



TheDealerTM::TheDealerTM()
{
}

void TheDealerTM::shuffleDeck()
{

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, cards.size() - 1);
	for (auto i = 0; i < cards.size(); ++i)
	{
		swap(cards.at(i), cards.at(dis(gen)));
	}
}

Card TheDealerTM::dealCard()
{
	Card auxiliaryCard = cards.back();
	cards.pop_back();
	return auxiliaryCard;
}


void TheDealerTM::printDeck()
{
	for (int index = 0; index < cards.size(); ++index)
		std::cout << cards[index];
}
void TheDealerTM::swap(Card & firstCard, Card & secondCard)
{
	Card auxiliary = firstCard;
	firstCard = secondCard;
	secondCard = auxiliary;
}

TheDealerTM::~TheDealerTM()
{
}
