#pragma once
#include "Deck.h"
class TheDealerTM :public Deck
{
public:
	TheDealerTM();
	~TheDealerTM();
	void shuffleDeck();
	Card dealCard();
	void printDeck();
	void swap(Card & firstCard, Card & secondCard);
private:
};

