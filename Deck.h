#pragma once
#include "Card.h"
#include <random>
#include <algorithm>
#include <vector>
class Deck
{
public:

	Deck();
	~Deck();
protected:
	/*static size_t numberOfCardsInDeck;*/
	static const size_t DeckSize = 81;
	
	std::vector<Card> cards;
};

