#pragma once
#include <iostream>
#include <string>
class Card
{
	
public:
	enum class Symbol
	{
		DIAMOND = 1 , 
		SQUIGGLE 	,
		OVAL	
	};
	enum class Shading
	{
		SOLID = 1,
		STRIPED ,
		OPEN
	};
	enum class Colour
	{
		RED = 1,
		GREEN,
		BLUE
	};
	Card(const int & number = 1 , const Symbol & symbol = Symbol::DIAMOND,const Shading & shading = Shading::SOLID,const Colour & colour = Colour::RED);
	Card(const Card & other);
	Card & operator = (const Card & other);
	std::string getShading()const;
	std::string getSymbol()const;
	std::string getColour()const;
	int getNumber()const;
	friend std::ostream& operator << (std::ostream& o, const Card& target);
	~Card();
private:
	int m_number;
	Symbol m_symbol;
	Shading m_shading;
	Colour m_colour;
};

