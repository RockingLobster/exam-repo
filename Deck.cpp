#include "Deck.h"


Deck::Deck()
{
	for (size_t index = 1; index < 4; ++index)
	{
		for (auto symbol = static_cast<int>(Card::Symbol::DIAMOND); symbol <= static_cast<int>(Card::Symbol::OVAL); symbol++)
		{
			for (auto shading = static_cast<int>(Card::Shading::SOLID); shading <= static_cast<int>(Card::Shading::OPEN); shading++)
			{
				for (auto colour = static_cast<int>(Card::Colour::RED); colour <= static_cast<int>(Card::Colour::BLUE); colour++)
				{
					cards.push_back(Card(index, static_cast<Card::Symbol>(symbol), static_cast<Card::Shading>(shading), static_cast<Card::Colour>(colour)));
				}
			}
		}
	}

}





Deck::~Deck()
{
}
